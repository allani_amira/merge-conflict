import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-angular-app';
  hello(){
    console.log("Hello world");
  } 
   calculate(x,y){
    console.log("x + y =", x+y);
  }
  allani(){
    console.log("Hello allani");
  } 
}


